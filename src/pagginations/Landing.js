import "./style/Landing.css";

function Landing() {
  return (
    <>
      <div
        id="carouselExampleIndicators"
        class="carousel slide contenedor-landing"
        data-bs-ride="carousel"
        data-bs-pause="true"
      >
        <div class="carousel-indicators">
          <button
            type="button"
            data-bs-target="#carouselExampleIndicators"
            data-bs-slide-to="0"
            class="active"
            aria-current="true"
            aria-label="Slide 1"
          ></button>
          <button
            type="button"
            data-bs-target="#carouselExampleIndicators"
            data-bs-slide-to="1"
            aria-label="Slide 2"
          >
            <span></span>
          </button>
          <button
            type="button"
            data-bs-target="#carouselExampleIndicators"
            data-bs-slide-to="2"
            aria-label="Slide 3"
          >
            <span></span>
          </button>
        </div>

        <div class="carousel-inner">
          <button class="btn-flotante">Leer mas ...</button>

          <div class="carousel-item active">
            <img
              src="./img/lan_1.jpg"
              class="img-fluid d-block img-centered w-100"
              alt="..."
            />
          </div>
          <div class="carousel-item">
            <img
              src="/img/lan_4.jpg"
              class="img-fluid d-block img-centered w-100"
              alt="..."
            />
          </div>
          <div class="carousel-item">
            <img
              src="/img/lan_2.jpg"
              class="img-fluid d-block img-centered w-100"
              alt="..."
            />
          </div>
        </div>
        <div className="palm">
          <img src="./img/palm.png" />
        </div>
        <h5 className="slogan">¡SOMOS LA PRIMERA OPCIÓN DE VARIOS CLIENTES!</h5>
      </div>
    </>
  );
}

export default Landing;
